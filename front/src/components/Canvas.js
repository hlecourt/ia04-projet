import React, { useState, useEffect } from 'react'
import axios from 'axios'

const Canvas = props => {
  const { width, height, start } = props
  let ctx, canvas

  useEffect(() => {
    ctx = canvas.getContext('2d')
    scale()
    ctx.strokeRect(0, 0, width, height)
  })

  useEffect(() => {
    console.log('coucou')
    const interval = setInterval(() => {
      axios
        .post(`http://localhost:8003/getFront`)
        .then(res => {
          let board = res.data.Board
          let xMax = board.length
          let yMax = board[0].length
          let drawXSize = width / xMax
          let drawYSize = height / yMax
          let rCircle = drawXSize / 2 - 5
          let y
          let x
          ctx.clearRect(0, 0, width, height)
          ctx.strokeRect(0, 0, width, height)
          board.map((valY, indexY) => {
            y = drawYSize / 2 + indexY * drawYSize
            valY.map((valX, indexX) => {
              x = drawXSize / 2 + indexX * drawXSize
              switch (valX.Content) {
                case 0:
                  ctx.beginPath()
                  if (valX.Fighter.Side === 1) {
                    ctx.fillStyle = 'black'
                  } else {
                    ctx.fillStyle = 'blue'
                  }

                  ctx.arc(x, y, rCircle, 0, 2 * Math.PI, false)
                  ctx.fill()

                  ctx.beginPath()
                  ctx.font = `${drawXSize / 2}px serif`
                  ctx.fillStyle = 'white'
                  switch (valX.Fighter.Type) {
                    case 0:
                      ctx.fillText('C', x - drawXSize / 5, y + drawXSize / 5)
                      break
                    case 1:
                      ctx.fillText('A', x - 10, y + 10)
                      break
                    case 2:
                      ctx.fillText('I', x - 10, y + 10)
                      break
                    default:
                      console.log('Prb type')
                  }
                  ctx.fill()
                  break
                case 1:
                  ctx.beginPath()
                  ctx.fillStyle = 'green'
                  ctx.fillRect(
                    drawXSize * indexX,
                    drawYSize * indexY,
                    drawXSize,
                    drawYSize
                  )
                  break
                case 2:
                  break
                default:
                  console.log(`Prb content`)
              }
            })
          })
        })
        .catch(() => console.log('Simulation pas encore lancée'))
    }, 300)
    return () => clearInterval(interval)
  }, [])

  const scale = () => {
    const ratio = window.devicePixelRatio || 1
    canvas.width = props.width * ratio
    canvas.height = props.height * ratio
    canvas.style.width = props.width + 'px'
    canvas.style.height = props.height + 'px'
    ctx.scale(ratio, ratio)
  }

  return <canvas ref={node => (canvas = node)} width={width} height={height} />
}

export default Canvas
