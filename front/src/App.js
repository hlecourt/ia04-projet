import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Canvas from './components/Canvas'
import './App.css'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'

const App = () => {
  const WIDTH = 800
  const HEIGHT = 800
  const [size, setSize] = useState('10')
  const [nbA, setNbA] = useState('10')
  const [nbB, setNbB] = useState('10')
  const [launched, setLaunched] = useState(false)
  const [percentA, setPercentA] = useState(['0.5', '0.4', '0.1'])
  const [percentB, setPercentB] = useState(['0.5', '0.4', '0.1'])
  const stop = () => {
    setLaunched(false)
    axios.post(`http://localhost:8003/shutdown`)
    axios.post(`http://localhost:8002/shutdown`)
  }
  const start = () => {
    setLaunched(true)
    axios.post(`http://localhost:8001/init`, {
      Board: parseInt(size),
      NbA: parseInt(nbA),
      NbB: parseInt(nbB),
      PA: [
        parseFloat(percentA[0]),
        parseFloat(percentA[1]),
        parseFloat(percentA[2])
      ],
      PB: [
        parseFloat(percentB[0]),
        parseFloat(percentB[1]),
        parseFloat(percentB[2])
      ]
    })
  }
  return (
    <div className='App'>
      <h1 className='title'>Simulateur champs de bataille </h1>
      <div className='container'>
        <div>
          <div className='text'>
            <TextField
              id='outlined-name'
              label='Size Board'
              value={size}
              onChange={event => setSize(event.target.value)}
            />
            <TextField
              id='outlined-name'
              label='Nombre soldat A'
              value={nbA}
              onChange={event => setNbA(event.target.value)}
            />
            <TextField
              id='outlined-name'
              label='Nombre soldat B'
              value={nbB}
              onChange={event => setNbB(event.target.value)}
            />
          </div>
          <div className='text'>
            <TextField
              id='outlined-name'
              label='% Chevalier A'
              value={percentA[0]}
              onChange={event =>
                setPercentA([event.target.value, percentA[1], percentA[2]])
              }
            />
            <TextField
              id='outlined-name'
              label='%  Archer A'
              value={percentA[1]}
              onChange={event =>
                setPercentA([percentA[0], event.target.value, percentA[2]])
              }
            />
            <TextField
              id='outlined-name'
              label='% Infirmier A'
              value={percentA[2]}
              onChange={event =>
                setPercentA([percentA[0], percentA[1], event.target.value])
              }
            />
          </div>
          <div className='text'>
            <TextField
              id='outlined-name'
              label='% Chevalier B'
              value={percentB[0]}
              onChange={event =>
                setPercentB([event.target.value, percentB[1], percentB[2]])
              }
            />
            <TextField
              id='outlined-name'
              label='% Archer B'
              value={percentB[1]}
              onChange={event =>
                setPercentB([percentB[0], event.target.value, percentB[2]])
              }
            />
            <TextField
              id='outlined-name'
              label='% Infirmier B'
              value={percentB[2]}
              onChange={event =>
                setPercentB([percentB[0], percentB[1], event.target.value])
              }
            />
          </div>
          <div className='button'>
            <Button variant='outlined' onClick={stop} disabled={!launched}>
              Stop
            </Button>
            <Button variant='outlined' onClick={start} disabled={launched}>
              Start
            </Button>
          </div>
        </div>

        <div className='Canvas'>
          <Canvas width={WIDTH} height={HEIGHT} start={launched} />
        </div>
      </div>
    </div>
  )
}

export default App
