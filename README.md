# IA04 - Battle Simulator

## Membres du groupe 
- Hugo Lecourt
- Boris Cazic
- Phuoc Khanh Le
- Luca Rougeron

## Description

Le but est de créer un champ de bataille entre deux populations. Nous avons décidé de limiter le nombre d'agents sur la carte ce qui permettra de faciliter le fait que chaque agent choisisse ses propres mouvements et les communique au serveur.

Pour rendre le problème plus intéressant, nous allons modéliser différents types de combattants ayant des attributs différents. Cette différence d'attributs va impacter la stratégie déployée par les agents (choix d'attaquer ou de fuir par exemple).

La carte sur laquelle les agents vont s'affronter contiendra des obstacles ce qui impactera la décision de mouvement de chaque agent.

## Enjeux de la simulation
Par cette simulation de bataille, nous cherchons à modéliser des comportements réalistes face à des risques de mort en créant des agents faisant leurs propres choix en fonction de leur environnement et de leur état (points de vie restants).


### Comportement général des agents après initialisation et lancement de la bataille
![](./agent_actions_diagram.png)

## Modélisation du champ de bataille (serveur/agent)
Nous avons décidé d'utiliser pour ce projet 2 serveurs différents avec lesquelles les agents communiquent : MapServer et BattleServer.

**Map Server**
Ce serveur est dédié au suivi de la positions des agents sur la carte. Les différents comportements a implémenté sont : 
- Au lancement du serveur, la carte est initialisé (aléatoirement ou pas).
- Lorsque un agent est initialisé, il demande au serveur une position initiale, le serveur renvoie une position ainsi que l'heure actuelle pour la synchronisation
- Tous les X secondes, les agents envoie au serveur sa position et le serveur renvoie l'environnement autour de l'agent.
- Lorsque un agent meurt, il le communique au serveur qui l'enlève de la carte.

Pour modélisé le champs de bataille, nous allons utilisé un grand quadrillage pour repérer les agents. Cela permet de faciliter la modélisation des déplacements des agents pendant la bataille.

**Battle Server**
Ce serveur est dédié au suivi des points de vie des combattants. Les différents comportements a implémenté sont :
- Lorsque chaque agent est initialisé, le serveur ajoute une value de HP par agent dans le map contenant les HP de tous les agents.
- Lorsqu'un agent attaque un ennemi, il envoie au serveur l'ID de l'attaqué, en fonction du type de l'attaquant, l'attaqué perd une partie de ses HP
- À la demande d'un agent, il lui envoie le nombre de HP qu'il lui reste.
- Le serveur doit stocké les évènements ayant eu lieu pendant la bataille.

# Pour aller plus loin
- Mettre en place probabilités de dégats (chaque agent a une précision)
- Stratégie calculatoire des agents : quand il se fait attaquer : calcule la probabilité de gagner un combat en fonction de :
    - la distance qui le sépare de son adversaire
    - les stats d'attaque de son adversaire en fonction de la probabilité de réussite d'attaque

# Setup
Deux serveurs à lancer donc sur deux terminaux différents :
- Back :
    - Dossier go/cmd
    - éxecuter : ```go run main_server.go```
- Front :
    - Dossier front
    - éxecuter : ```npm start```

Pour visualiser le front : ```http://localhost:3000``` (navigateur)

Erreurs possibles : 
- Potentielle désynchronisation entre le front et le back 
- Potentielle problème de communication

Solution : relancer le serveur back + rafraîchir le navigateur

Toujours un problème ?
- Vérifier que les ports 3000, 8001, 8002 et 8003 soient ouvert

Si le problème persiste après ouverture les ports 8001 à 8003 changer les url des serveurs dans le fichier ```go/cmd/main_server.go``` et ```go/server/server_main.go``` :

```go
go/cmd/main_server.go :
l11 - servMain := srv.NewRestMainServerAgent(":8001")    
```
```go
go/server/server_main.go
l71 - const urlPos_port = ":8003"
l72 - const urlPos = "http://localhost:8003"
l73 - const urlBattle_port = ":8002"
l74 - const urlBattle = "http://localhost:8002"
```

# Simulation 

La simulation permet de faire varier différents paramètres :

- Size Board
- Nombre soldat A
- Nombre Soldat B 
- Répartition du type de soldat dans le camp A et B en pourcentage (la somme des trois ne doit pas dépasser 1)

Un fois les paramètres modifiés on peut lancer la simulation en appuyant sur le bouton start et l'arreter avec le bouton stop. (Si un prolème survient se réfreré à la section précédente)

Deux camps modélisés par deux couleurs noir et bleu.
Trois type d'agent modéliser par une lettre :
- C - Chevalier
- A - Archer
- I - Infirmier
