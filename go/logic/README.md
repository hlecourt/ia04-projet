# Logique des agents

## Structures 
-  typeAction : {0 : déplacement, 1 : attaque, 2 : fuite, 3 : nil}

## Fonction à implémenter
### Pour tous les agents
- [x] distance entre un objet  : la range est représentée par un carré (cercle trop compliqué à modélisé pour des int)
- [x] ennemiesSightRange(agent agt.Agent) (int, []agt.Fighter, error)
- [x] ennemiesAttackRange(agent agt.Agent) (int, []agt.Fighter, error)
- [] fonction d'actions : (soit fonctions soit méthodes) à définir dans package agt si méthode
    - [] Attack(id,agt)
    - [] Move(position, agt)
    - [] retreat(position,agt)
    - [] Move_Toward(ennemi, agt) ? si on fait une possibilité de suivre qqn
- [] Fonctions d'analyse de situation : (prend en compte HP restant et ennemis dans l'environnement)

### Choix à faire
- pour les déplacements : 
    - définir la méthode (en fonction de speed ?)
    - quand l'ordre de déplacement est lancée : l'agent va à la position sans s'arrêter ou il réévalue son environnement à intervalle régulier (possibilité de suivre un autre agent qui reste dans sa zone de vision)
    - quand il se déplace : faire des envois de position au serveur le long du chemin pour mettre à jour sa position

### Stratégies possibles pour chaque situation
- Attaque :
    - le plus proche
    - le plus faible
- Déplacement :
    - Fuite
    - Rapprochement d'un ennemie
    - Avancer pour trouver un ennemi
    - ? se mettre à couvert ? (sûrement trop dur)

## <span style="color:red">**Chevalier**</span>
### Cas 1 : 

- Env :
    - Pas d'ennemi dans range view
    - Pas d'ennemi dans range attack
- Stratégie :
    - Déplacement vers side opposé

### Cas 2 :

- Env :
    - Ennemi dans range view
    - Pas d'ennemi dans range attack
- Stratégie :
    - Déplacement vers l'ennemi.e

### Cas 3 :

- Env :
    - Ennemi dans range view (<= 5)
    - Ennemi dans range attack (== 1)
- Stratégie :
    - Si assez d'HP (fixer seuil) :
        - Attack
    - Sinon :
        - Si fuite possible :
            - Fuire
        - Sinon : 
            - Attack


### Cas 4 :

- Env :
    - Plus d'1 ennemi dans range view
    - Plus d'1 ennemi dans range attack
- Stratégie : 
    - Si l'un des ennemis est du type infirmier : 
        - L'attaquer lui 
    - Sinon :
        - Attaquer l'ennemi le plus proche

### Cas 5 :

- Env : 
    - Peut importe
    - Perte d'HP
- Stratégie :
    - Identification de l'ennemi
    - Prise de décision (en fonction de l'env et de l'HP restant)
        - Move (ennemi trop loin = essaie de skip la range)
        - Attack (ennemi assez prêt = deplacement vers celui-ci)
        - Si pas assez d'HP : fuite et recherche d'infirmier

### Cas 7 :

- Env :
    - Obstacle
- Stratégie :
    - Move dans direction !=

### Cas 8 :

- Env :
    - Infirmier présent dans range view
- Stratégie :
    - Si HP != MaxHP
        - Move vers l'infirmier

### Cas 9 :

- Env :
    - Infirmier en train de le soigner
- Stratégie :
    - Si attack d'un ennemi
        - Fuite

## <span style="color:red">**Archer**</span>
#### Cas 1 : 

- Env :
    - Pas d'ennemi dans range view
    - Pas d'ennemi dans range attack
- Stratégie :
    - Déplacement vers side opposé

#### Cas 2 :

- Env :
    - Ennemi dans range view
    - Pas d'ennemi dans range attack
- Stratégie :
    - Déplacement vers l'ennemi

#### Cas 3 :

- Env :
    - Ennemi dans range view (> 2 positions)
    - Ennemi dans range attack
- Stratégie :
    - Attaque l'ennemi 

#### Cas 4 :

- Env :
    - Ennemi dans range view (proche environ 2 positions)
    - Ennemi dans range attack
- Stratégie :
    - Fuite 

#### Cas 5 :

- Env : 
    - Peut importe
    - Perte d'HP
- Stratégie :
    - Identification de l'ennemi
    - Prise de décision (en fonction de l'env et de l'HP restant)
        - Move (ennemi trop loin = essaie de skip la range)
        - Attack (ennemi assez prêt = deplacement vers celui-ci)
        - Si pas assez d'HP : fuite et recherche d'infirmier

#### Cas 6 :

- Env :
    - Obstacle
- Stratégie :
    - Move dans direction !=

#### Cas 7 :

- Env :
    - Infirmier présent dans range view
- Stratégie :
    - Si HP != MaxHP
        - Move vers l'infirmier

#### Cas 8 :

- Env :
    - Infirmier en train de le soigner
- Stratégie :
    - Si attack d'un ennemi
        - Fuite


## <span style="color:red">**Infirmier**</span>

#### Cas 1 :

- Env : 
    - Pas d'allier dans range view
    - Pas d'ennemi dans range view
- Strategie :
    - Move

#### Cas 2 :

- Env : 
    - Pas d'allier dans range view
    - Ennemi dans range view
- Strategie :
    - Fuite

#### Cas 3 :

- Env : 
    - Allier dans range view
    - Pas d'ennemi dans range view
- Strategie :
    - Si allier a besoin d'être soigné (getHp()) :
        - Soigner (negative damage) tant que HP != MaxHP
    - Sinon : 
        - Move

#### Cas 4 :

- Env : 
    - En train de soigner un allier
- Stratégie :
    - Si ennemi en train d'attack :
        - Fuite

