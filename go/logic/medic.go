package logic

import (
	agt "ia04-projet/go/agt"
)

func logic_Medic(agent agt.Agent, mapHP map[agt.AgentID]int) (Action, error) {
	// number_allies_sight, _, err_allies_sight := alliesSightRange(agent)
	// number_allies_att, _, err_allies_att := alliesAttackRange(agent)
	list_pos_ennemies_sight, list_pos_ennemies_attack, list_pos_allies_sight, list_pos_allies_attack := analyseEnv(agent)

	//check if errors
	// if err_allies_sight != nil || err_allies_att != nil {
	// 	return action, errors.New("erreur fonctions de detection env")
	// }

	//début de logique
	if len(list_pos_ennemies_sight) == 0 && len(list_pos_allies_sight) == 0 {
		return moveAdvance(agent), nil
	} else {
		if len(list_pos_allies_sight) > 0 || len(list_pos_ennemies_sight) > 0 {
			if len(list_pos_allies_sight) == 0 && len(list_pos_ennemies_sight) > 0 {
				return retreat(agent), nil
			} else {
				if len(list_pos_allies_attack) >= len(list_pos_ennemies_attack) {
					_, target, found := nearestAllyNeedingHealth(agent, list_pos_allies_sight, mapHP)
					if found {
						return heal(target), nil
					} else {
						return moveAdvance(agent), nil
					}
				} else {
					pos_closest, _, found := nearestAllyNeedingHealth(agent, list_pos_allies_sight, mapHP)
					if found {
						return moveByDirection(agent, pos_closest), nil
					} else {
						return retreat(agent), nil
					}
				}
			}
		} else {
			return moveAdvance(agent), nil
		}
	}
}
