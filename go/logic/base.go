package logic

import (
	"errors"
	"fmt"
	agt "ia04-projet/go/agt"
	"math"
)

type TypeAction int

const (
	MOVE    TypeAction = 0
	ATTACK  TypeAction = 1
	RETREAT TypeAction = 2
	NOTHING TypeAction = 3
	HEAL    TypeAction = 4
	// ... //
)

type Action struct {
	Type    TypeAction
	NextPos agt.Position // si typeAction = 0 (déplacement)
	Target  agt.AgentID  // si typeAction = 1 (attaque) ou typeAction =4 (heal)
}

func GetAction(agent agt.Agent, mapHP map[agt.AgentID]int) (Action, error) {
	var act Action
	var err error
	switch agent.Type.Type {
	case 0:
		act, err = logic_Chevalier(agent)
	case 1:
		act, err = logic_Archer(agent)
	case 2:
		act, err = logic_Medic(agent, mapHP)
	default:
		fmt.Println("Default statement executed")
	}
	return act, err
}

func distance(a agt.Position, b agt.Position) int {
	return int(math.Max(math.Abs(float64(a.X)-float64(b.X)),
		math.Abs(float64(a.Y)-float64(b.Y))))
}

// func ennemiesSightRange(agent agt.Agent) (int, []agt.Fighter, error) {
// 	slice_ennemis := make([]agt.Fighter, 0)
// 	number_ennemies := 0
// 	if agent.Env == nil {
// 		return 0, slice_ennemis, errors.New("environnement agent nul")
// 	}

// 	for _, content := range agent.Env {
// 		if content.Content == 0 && content.Fighter.Side != agent.Side {
// 			number_ennemies++
// 			slice_ennemis = append(slice_ennemis, content.Fighter)
// 		}

// 	}
// 	return number_ennemies, slice_ennemis, nil
// }

// func ennemiesAttackRange(agent agt.Agent) (int, []agt.Fighter, error) {
// 	number_ennemies := 0
// 	slice_ennemis := make([]agt.Fighter, 0)
// 	if agent.Env == nil {
// 		return 0, slice_ennemis, errors.New("environnement agent nul")
// 	}

// 	for pos, content := range agent.Env {
// 		if content.Content == 0 && content.Fighter.Side != agent.Side {
// 			if distance(pos, agent.Position) <= agent.Type.AttackRange {
// 				number_ennemies++
// 				slice_ennemis = append(slice_ennemis, content.Fighter)
// 			}
// 		}
// 	}
// 	return number_ennemies, slice_ennemis, nil
// }

func alliesSightRange(agent agt.Agent) (int, []agt.Fighter, error) {
	slice_allies := make([]agt.Fighter, 0)
	number_allies := 0
	if agent.Env == nil {
		return number_allies, slice_allies, errors.New("environnement agent nul")
	}

	for _, content := range agent.Env {
		if content.Content == 0 && content.Fighter.Side == agent.Side {
			number_allies++
			slice_allies = append(slice_allies, content.Fighter)
		}
	}
	return number_allies, slice_allies, nil
}

func alliesAttackRange(agent agt.Agent) (int, []agt.Fighter, error) {
	slice_allies := make([]agt.Fighter, 0)
	number_allies := 0
	if agent.Env == nil {
		return number_allies, slice_allies, errors.New("environnement agent nul")
	}

	for pos, content := range agent.Env {
		if content.Content == 0 && content.Fighter.Side == agent.Side {
			if distance(pos, agent.Position) <= agent.Type.AttackRange {
				number_allies++
				slice_allies = append(slice_allies, content.Fighter)
			}
		}
	}
	return number_allies, slice_allies, nil
}

func analyseEnv(agent agt.Agent) ([]agt.Position, []agt.Position, []agt.Position, []agt.Position) {
	list_pos_ennemies_sight := make([]agt.Position, 0)
	list_pos_ennemies_attack := make([]agt.Position, 0)
	list_pos_allies_sight := make([]agt.Position, 0)
	list_pos_allies_attack := make([]agt.Position, 0)
	for pos, content := range agent.Env {
		if content.Content == 0 {
			if content.Fighter.Side == agent.Side {
				if distance(pos, agent.Position) <= agent.Type.AttackRange {
					list_pos_allies_attack = append(list_pos_allies_attack, pos)
				}
				list_pos_allies_sight = append(list_pos_allies_sight, pos)
			} else {
				if distance(pos, agent.Position) <= agent.Type.AttackRange {
					list_pos_ennemies_attack = append(list_pos_ennemies_attack, pos)
				}
				list_pos_ennemies_sight = append(list_pos_ennemies_sight, pos)
			}

		}
	}
	return list_pos_ennemies_sight, list_pos_ennemies_attack, list_pos_allies_sight, list_pos_allies_attack
}

func attack(agent agt.Agent, ennId agt.AgentID) Action {
	return Action{Type: 1, Target: ennId}
}

func heal(allyID agt.AgentID) Action {
	return Action{Type: 4, Target: allyID}
}

func isBarrier(agent agt.Agent, pos agt.Position) bool {
	if agent.Env[pos].Fighter == agt.NilFighter() {
		return false
	} else if agent.Env[pos].Content == agt.TypeContent(1) || agent.Env[pos].Content == agt.TypeContent(0) {
		fmt.Println(agent.Env[pos])
		fmt.Println("get here")
		return true
	} else {
		return false
	}
}

func moveAdvance(agent agt.Agent) Action {
	var nextPos agt.Position
	var X int
	if agent.Side == 1 {
		X = agent.Position.X + 1
		nextPos = agt.Position{X: X, Y: agent.Position.Y}
	} else {
		X = agent.Position.X - 1
		nextPos = agt.Position{X: X, Y: agent.Position.Y}
	}
	if isBarrier(agent, nextPos) {
		if agt.RandomTrueByPercentage(0.5) {
			nextPos = agt.Position{X: X, Y: agent.Position.Y + 1}
		} else {
			nextPos = agt.Position{X: X, Y: agent.Position.Y - 1}
		}
	}
	if isBarrier(agent, nextPos) {
		if agt.RandomTrueByPercentage(0.5) {
			nextPos = agt.Position{X: agent.Position.X, Y: agent.Position.Y + 1}
		} else {
			nextPos = agt.Position{X: agent.Position.X, Y: agent.Position.Y - 1}
		}
	}
	return Action{Type: 0, NextPos: nextPos}
}

func moveByDirection(agent agt.Agent, dst agt.Position) Action {
	NextPos := agent.Position
	if agent.Position.X == dst.X {
		if agent.Position.Y > dst.Y {
			NextPos.Y = NextPos.Y - 1
		} else if agent.Position.Y < dst.Y {
			NextPos.Y = NextPos.Y + 1
		} else {
			return Action{}
		}
	} else if agent.Position.X < dst.X {
		if agent.Position.Y > dst.Y {
			NextPos.Y = NextPos.Y - 1
			NextPos.X = NextPos.X + 1
		} else if agent.Position.Y < dst.Y {
			NextPos.Y = NextPos.Y + 1
			NextPos.X = NextPos.X + 1
		} else {
			NextPos.X = NextPos.X + 1
		}
	} else {
		if agent.Position.Y > dst.Y {
			NextPos.Y = NextPos.Y - 1
			NextPos.X = NextPos.X - 1
		} else if agent.Position.Y < dst.Y {
			NextPos.Y = NextPos.Y + 1
			NextPos.X = NextPos.X - 1
		} else {
			NextPos.X = NextPos.X - 1
		}
	}
	return Action{Type: 0, NextPos: NextPos}
}

func nearestEnnemy(agent agt.Agent, list_pos_ennemies_sight []agt.Position) (agt.Position, agt.AgentID) {
	curDist := 1000000000000
	var curAgentId agt.AgentID
	var target agt.Position
	var dist int

	for _, pos := range list_pos_ennemies_sight {
		dist = distance(agent.Position, pos)
		if dist < curDist {
			curDist = dist
			curAgentId = agent.Env[pos].Fighter.ID
			target = pos
		}
	}
	return target, curAgentId
}

func nearestAllyNeedingHealth(agent agt.Agent, list_pos_ally []agt.Position, mapHP map[agt.AgentID]int) (agt.Position, agt.AgentID, bool) {
	curDist := 1000000000000
	var curAgentId agt.AgentID
	var target agt.Position
	var dist int
	found := false

	for _, pos := range list_pos_ally {
		dist = distance(agent.Position, pos)
		if mapHP[agent.Env[pos].Fighter.ID] < 100 { // seulement ceux qui n'ont pas de PVmax
			if dist < curDist {
				curDist = dist
				curAgentId = agent.Env[pos].Fighter.ID
				target = pos
				found = true
			}
		}
	}
	return target, curAgentId, found
}

const PERCENT_MIN_HP = 0.2

func isEnoughHp(agent agt.Agent) bool {
	return float64(agent.Health) >= PERCENT_MIN_HP*float64(agent.Type.MaxHealth)
}

// func isRetreatPossible(agent agt.Agent) bool {
// 	if agent.Side == 1 {
// 		NextPos = agt.Position{X: agent.Position.X + 1, Y: agent.Position.Y}
// 	} else {
// 		NextPos = agt.Position{X: agent.Position.X - 1, Y: agent.Position.Y}
// 	}
// }

func retreat(agent agt.Agent) Action {
	var NextPos agt.Position
	if agent.Side == 1 {
		NextPos = agt.Position{X: agent.Position.X - 1, Y: agent.Position.Y}
	} else {
		NextPos = agt.Position{X: agent.Position.X + 1, Y: agent.Position.Y}
	}
	if agent.Env[NextPos].Content == agt.TypeContent(1) {
		NextPos.Y = NextPos.Y - 1
	}
	if agent.Env[NextPos].Content == agt.TypeContent(1) {
		NextPos.Y = NextPos.Y + 2
	}
	return Action{Type: 0, NextPos: NextPos}
}
