package logic

import (
	agt "ia04-projet/go/agt"
)

func logic_Chevalier(agent agt.Agent) (Action, error) {
	list_pos_ennemies_sight, list_pos_ennemies_attack, list_pos_allies_sight, _ := analyseEnv(agent)

	if len(list_pos_ennemies_sight) == 0 {
		if agent.Health < agent.Type.MaxHealth {
			for _, pos := range list_pos_allies_sight {
				if agent.Env[pos].Fighter.Side == agent.Side && agent.Env[pos].Fighter.Type == agt.TypeSoldier(2) {
					return moveByDirection(agent, pos), nil
				}
			}
			return moveAdvance(agent), nil
		} else {
			return moveAdvance(agent), nil
		}
	} else {
		if len(list_pos_ennemies_attack) == 0 {
			nearestEnnPos, _ := nearestEnnemy(agent, list_pos_ennemies_sight)
			return moveByDirection(agent, nearestEnnPos), nil
		} else if len(list_pos_ennemies_attack) == 1 {
			if isEnoughHp(agent) {
				return attack(agent, agent.Env[list_pos_ennemies_attack[0]].Fighter.ID), nil
			} else {
				return retreat(agent), nil
			}
		} else {
			for _, en_pos := range list_pos_ennemies_attack {
				if agent.Env[en_pos].Fighter.Type == agt.TypeSoldier(2) {
					return attack(agent, agent.Env[en_pos].Fighter.ID), nil
				}
			}
			_, nearestEnnId := nearestEnnemy(agent, list_pos_ennemies_sight)
			return attack(agent, nearestEnnId), nil
		}
	}
}
