package client

import (
	"fmt"
	"ia04-projet/go/agt"
	"net/http"
)

type RestClient struct {
	agent     agt.Agent
	urlPos    string
	urlBattle string
	isDead    bool
}

func NewRestClient(agent agt.Agent, urlPos string, urlBattle string) *RestClient {
	return &RestClient{agent: agent, urlPos: urlPos, urlBattle: urlBattle, isDead: false}
}

func (rca *RestClient) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}
