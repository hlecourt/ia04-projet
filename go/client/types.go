package client

import (
	agt "ia04-projet/go/agt"
)

type RequestPositionInit struct {
	ID   agt.AgentID
	Side int
	Type int
}

type ReponsePositionInit struct {
	Pos agt.Position
}

type RequestInitHP struct {
	Id   agt.AgentID
	Hp   int
	Side int
}

type RequestGetHP struct {
	Id   agt.AgentID
	Side int
}

type ResponseGetHP struct {
	Hp int
}

type ResquestAttack struct {
	Id     agt.AgentID
	Damage int
	Side   int
}

type RequestHeal struct {
	Id      agt.AgentID
	MaxHeal int
	Side    int
}

type RequestUpdateHp struct {
	Id agt.AgentID
}

type ResponseUpdateHp struct {
	HP int
}

type ResquestDeath struct {
	Id   agt.AgentID
	Side int
}
type RequestGetEnv struct {
	ID         agt.AgentID
	SightRange int
}

type ResponseGetEnv struct {
	List_pos []agt.Position
	List_cnt []agt.CellContent
}

type RequestMove struct {
	ID  agt.AgentID
	Pos agt.Position
}

type ReponseMove struct {
	IsAllow bool
}

type ResponseGetFront struct {
	Board [][]agt.CellContent
}

type RequestInit struct {
	Board int
	NbA   int
	NbB   int
	PA    [3]float32
	PB    [3]float32
}

type RequestGetMapHP struct {
	Id agt.AgentID
}

type ResponseGetMapHP struct {
	MapHP map[agt.AgentID]int
}
