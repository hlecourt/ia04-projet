package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"ia04-projet/go/agt"
	"net/http"
)

func (rca *RestClient) treatResponseMoveAdvance(r *http.Response) bool {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ReponseMove
	json.Unmarshal(buf.Bytes(), &resp)

	return resp.IsAllow
}

func (rca *RestClient) doRequestMoveAdvance(pos agt.Position) {
	req := RequestMove{
		ID:  rca.agent.ID,
		Pos: pos,
	}

	// sérialisation de la requête
	url := rca.urlPos + "/move"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	isAllow := rca.treatResponseMoveAdvance(resp)
	if isAllow {
		rca.agent.Position = pos
		fmt.Println(rca.agent.ID, " current position ", rca.agent.Position)
	}

}

func (rca *RestClient) treatResponseMoveRetreat(r *http.Response) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ResponseGetEnv
	json.Unmarshal(buf.Bytes(), &resp)
}

func (rca *RestClient) doRequestMoveRetreat() {

}
