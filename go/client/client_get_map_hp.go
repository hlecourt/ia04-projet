package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"ia04-projet/go/agt"
	"net/http"
)

func (rca *RestClient) treatResponseGetMapHP(r *http.Response) map[agt.AgentID]int {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ResponseGetMapHP
	json.Unmarshal(buf.Bytes(), &resp)

	return resp.MapHP
}

func (rca *RestClient) doRequestGetMapHP() map[agt.AgentID]int {
	req := RequestGetMapHP{
		Id: rca.agent.ID,
	}
	var MapHP map[agt.AgentID]int

	// sérialisation de la requête
	url := rca.urlBattle + "/mapHP"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return MapHP
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return MapHP
	}

	MapHP = rca.treatResponseGetMapHP(resp)
	return MapHP
}
