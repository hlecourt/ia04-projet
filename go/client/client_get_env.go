package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func (rca *RestClient) treatResponseGetEnv(r *http.Response) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ResponseGetEnv
	json.Unmarshal(buf.Bytes(), &resp)

	for key := range rca.agent.Env {
		delete(rca.agent.Env, key)
	}

	for index, pos := range resp.List_pos {
		if pos != rca.agent.Position {
			rca.agent.Env[pos] = resp.List_cnt[index]
		}
	}
}

func (rca *RestClient) doRequestGetEnv() {
	req := RequestGetEnv{
		ID:         rca.agent.ID,
		SightRange: rca.agent.Type.SightRange,
	}

	// sérialisation de la requête
	url := rca.urlPos + "/getEnv"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	rca.treatResponseGetEnv(resp)
}
