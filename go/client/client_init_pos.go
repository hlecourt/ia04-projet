package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func (rca *RestClient) treatResponsePositionInit(r *http.Response) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ReponsePositionInit
	json.Unmarshal(buf.Bytes(), &resp)

	rca.agent.Position = resp.Pos
}

func (rca *RestClient) doRequestPositionInit() {
	req := RequestPositionInit{
		ID:   rca.agent.ID,
		Side: rca.agent.Side,
		Type: int(rca.agent.Type.Type),
	}

	// sérialisation de la requête
	url := rca.urlPos + "/initPos"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		fmt.Println(err.Error())
		return
	}

	rca.treatResponsePositionInit(resp)
	fmt.Println(rca.agent.ID, " : position current is ", rca.agent.Position)
}
