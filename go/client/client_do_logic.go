package client

import (
	"fmt"
	"ia04-projet/go/agt"
	"ia04-projet/go/logic"
	"math"
	"time"
)

func dist(src agt.Position, dst agt.Position) int {
	return int(math.Max(float64(src.X-dst.X), float64(src.Y-dst.Y)))
}

func (rca *RestClient) doLogic() {
	rca.updateHp()
	if rca.isDead {
		fmt.Println(rca.agent.ID, "death")
		rca.doDeath()
		return
	}
	rca.doRequestGetEnv()

	mapHP := make(map[agt.AgentID]int)
	if rca.agent.Type.Type == 2 {
		mapHP = rca.doRequestGetMapHP()
	}
	action, err := logic.GetAction(rca.agent, mapHP)

	if err != nil {
		return
	}

	switch action.Type {
	case logic.TypeAction(0):
		rca.doRequestMoveAdvance(action.NextPos)
		break
	case logic.TypeAction(1):
		rca.doRequestAttack(action.Target)
		break
	case logic.TypeAction(2):
		rca.doRequestMoveRetreat()
		break
	case logic.TypeAction(3):
		break
	case logic.TypeAction(4):
		rca.doRequestHeal(action.Target)
		break
	}
	time.Sleep(300 * time.Millisecond)

	rca.doLogic()
}
