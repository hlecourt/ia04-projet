package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	agt "ia04-projet/go/agt"
	"net/http"
)

func (rca *RestClient) treatResponseGetHp(r *http.Response) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ResponseGetHP
	json.Unmarshal(buf.Bytes(), &resp)

	rca.agent.Health = resp.Hp
}

func (rca *RestClient) treatResponseGetSpecificHp(r *http.Response) int {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ResponseGetHP
	json.Unmarshal(buf.Bytes(), &resp)

	return resp.Hp
}

func (rca *RestClient) doRequestGetHp() {
	req := RequestGetHP{
		Id:   rca.agent.ID,
		Side: rca.agent.Side,
	}

	// sérialisation de la requête
	url := rca.urlBattle + "/getHP"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	rca.treatResponseGetHp(resp)
	fmt.Println(rca.agent.ID, ": hp current is", rca.agent.Health)
}

func (rca *RestClient) doRequestGetSpecificHp(agent agt.Agent) int {
	req := RequestGetHP{
		Id:   agent.ID,
		Side: agent.Side,
	}

	// sérialisation de la requête
	url := rca.urlBattle + "/getHP"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return -1
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return -1
	}

	return rca.treatResponseGetSpecificHp(resp)
}
