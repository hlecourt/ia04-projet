package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"ia04-projet/go/agt"
	"net/http"
)

func (rca *RestClient) doRequestHeal(Agent agt.AgentID) {
	req := RequestHeal{
		Id:      Agent,
		MaxHeal: rca.agent.Type.Damage,
		Side:    rca.agent.Side,
	}

	// sérialisation de la requête
	url := rca.urlBattle + "/heal"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
}
