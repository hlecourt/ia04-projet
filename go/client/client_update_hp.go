package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func (rca *RestClient) treatUpdateHpResponse(r *http.Response) int {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp ResponseUpdateHp
	json.Unmarshal(buf.Bytes(), &resp)

	return resp.HP

}

func (rca *RestClient) updateHp() {
	req := RequestUpdateHp{
		Id: rca.agent.ID,
	}

	url := rca.urlBattle + "/updateHp"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	rca.agent.Health = rca.treatUpdateHpResponse(resp)

	if rca.agent.Health <= 0 {
		rca.isDead = true
	}
}
