package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func (rca *RestClient) doRequestBattleInit() {
	req := RequestInitHP{
		Id:   rca.agent.ID,
		Hp:   rca.agent.Health,
		Side: rca.agent.Side,
	}

	// sérialisation de la requête
	url := rca.urlBattle + "/initHP"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		_ = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
}
