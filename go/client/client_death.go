package client

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func (rca *RestClient) doDeath() {
	req := ResquestDeath{
		Id:   rca.agent.ID,
		Side: rca.agent.Side,
	}

	// sérialisation de la requête
	url_pos := rca.urlPos + "/death"
	url_bat := rca.urlBattle + "/death"
	data, _ := json.Marshal(req)

	// envoi de la requête
	_, err := http.Post(url_pos, "application/json", bytes.NewBuffer(data))
	_, err = http.Post(url_bat, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
}
