package client

import "log"

func (rca *RestClient) Start() {
	log.Printf("démarrage de %s", rca.agent.ID)
	rca.doRequestPositionInit()
	rca.doRequestBattleInit()
	rca.updateHp()
	rca.doLogic()
}
