package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	agt "ia04-projet/go/agt"
	clt "ia04-projet/go/client"
	"net/http"
)

func (*RestMapServer) decodeRequestMove(r *http.Request) (req clt.RequestMove, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestMapServer) getPermissionMove(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestMove(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	var resp clt.ReponseMove

	if req.Pos.X < 0 || req.Pos.X >= rsa.sizeBoard || req.Pos.Y < 0 || req.Pos.Y >= rsa.sizeBoard {
		resp.IsAllow = false
	} else if rsa.board[req.Pos.X][req.Pos.Y].Content == 2 {
		lastPos := rsa.posManager[req.ID]
		rsa.board[req.Pos.X][req.Pos.Y] = rsa.board[lastPos.X][lastPos.Y]
		rsa.posManager[req.ID] = req.Pos
		rsa.board[lastPos.X][lastPos.Y] = agt.CellContent{Content: 2, Fighter: agt.NilFighter()}
		resp.IsAllow = true
	} else {
		resp.IsAllow = false
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}
