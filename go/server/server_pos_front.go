package server

import (
	"encoding/json"
	clt "ia04-projet/go/client"
	"net/http"
)

func (rsa *RestMapServer) getFront(w http.ResponseWriter, r *http.Request) {

	rsa.Lock()
	defer rsa.Unlock()

	setupResponse(&w, r)

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	var resp clt.ResponseGetFront
	resp.Board = rsa.board

	//fmt.Println(resp.Board)
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}
