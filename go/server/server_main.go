package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	agt "ia04-projet/go/agt"
	clt "ia04-projet/go/client"
	"log"
	"net/http"
	"sync"
	"time"
)

type RestMainServer struct {
	sync.Mutex
	id   string
	addr string
}

func NewRestMainServerAgent(addr string) *RestMainServer {
	return &RestMainServer{id: addr, addr: addr}
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

// Test de la méthode
func (rsa *RestMainServer) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (*RestMainServer) decodeRequestInit(r *http.Request) (req clt.RequestInit, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestMainServer) init(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	setupResponse(&w, r)
	if r.Method == "OPTIONS" {
		return
	}
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	fmt.Println(r)

	req, err := rsa.decodeRequestInit(r)
	fmt.Println(req, err)

	nb_soliders_A := req.NbA
	nb_soliders_B := req.NbB
	partition_A := req.PA
	partition_B := req.PB
	fmt.Println(nb_soliders_A, nb_soliders_B, partition_A, partition_B)

	const urlPos_port = ":8003"
	const urlPos = "http://localhost:8003"
	const urlBattle_port = ":8002"
	const urlBattle = "http://localhost:8002"

	listAgtSolidats_A := make([]clt.RestClient, 0, nb_soliders_A)
	listAgtSolidats_B := make([]clt.RestClient, 0, nb_soliders_B)
	servAgt := NewRestPosServerAgent(urlPos_port, req.Board)
	servBattle := NewRestBattleServerAgent(urlBattle_port)

	log.Println("démarrage serveur pos...")
	go servAgt.Start()

	log.Println("démarrage serveur battle...")
	go servBattle.Start()

	log.Println("démarrage des solidats...")
	jA := 0
	nb_passA := 0
	for i := 0; i < nb_soliders_A; i++ {
		id := fmt.Sprintf("idA%02d", i)

		if int(float32(nb_soliders_A)*partition_A[jA])+nb_passA == i && jA <= 1 {
			nb_passA += int(float32(nb_soliders_A) * partition_A[jA])
			fmt.Println("je rentre A", i)
			jA += 1
		}
		agtSolider := agt.NewAgentSoldier(agt.AgentID(id), agt.TypeSoldier(jA), 1)
		listAgtSolidats_A = append(listAgtSolidats_A, *clt.NewRestClient(agtSolider, urlPos, urlBattle))
	}

	jB := 0
	nb_passB := 0
	for i := 0; i < nb_soliders_B; i++ {
		id := fmt.Sprintf("idB%02d", i)

		if int(float32(nb_soliders_B)*partition_B[jB])+nb_passB == i && jB <= 1 {
			nb_passB += int(float32(nb_soliders_B) * partition_B[jB])
			fmt.Println("je rentre B", i)
			jB += 1
		}
		agtSolider := agt.NewAgentSoldier(agt.AgentID(id), agt.TypeSoldier(jB), 2)
		listAgtSolidats_B = append(listAgtSolidats_B, *clt.NewRestClient(agtSolider, urlPos, urlBattle))
	}

	for _, a := range listAgtSolidats_A {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		func(a clt.RestClient) {
			go a.Start()
		}(a)
	}

	for _, a := range listAgtSolidats_B {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		func(a clt.RestClient) {
			go a.Start()
		}(a)
	}
}

func (rsa *RestMainServer) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	mux.HandleFunc("/init", rsa.init)

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
