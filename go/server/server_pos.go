package server

import (
	"context"
	"fmt"
	agt "ia04-projet/go/agt"
	"log"
	"net/http"
	"sync"
	"time"
)

const PERCENT_OBSTACLE = 0.1

type RestMapServer struct {
	sync.Mutex
	id            string
	addr          string
	board         [][]agt.CellContent
	nb_soldiers_A int
	nb_soldiers_B int
	sizeBoard     int
	posManager    map[agt.AgentID]agt.Position
}

func NewRestPosServerAgent(addr string, sizeBoard int) *RestMapServer {

	posManager := make(map[agt.AgentID]agt.Position)

	board := make([][]agt.CellContent, sizeBoard)
	for i := range board {
		board[i] = make([]agt.CellContent, sizeBoard)
	}

	for i := 0; i < sizeBoard; i++ {
		for j := 0; j < sizeBoard; j++ {
			if agt.RandomTrueByPercentage(PERCENT_OBSTACLE) {
				board[i][j] = agt.CellContent{Content: 1, Fighter: agt.NilFighter()}
			} else {
				board[i][j] = agt.CellContent{Content: 2, Fighter: agt.NilFighter()}
			}
		}
	}
	return &RestMapServer{id: addr, addr: addr, board: board,
		nb_soldiers_A: 0, nb_soldiers_B: 0, sizeBoard: sizeBoard, posManager: posManager}
}

// Test de la méthode
func (rsa *RestMapServer) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (rsa *RestMapServer) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	// création du serveur http
	s := http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	mux.HandleFunc("/initPos", rsa.initPosition)
	mux.HandleFunc("/getEnv", rsa.getEnv)
	mux.HandleFunc("/move", rsa.getPermissionMove)
	mux.HandleFunc("/death", rsa.treatDeath)
	mux.HandleFunc("/getFront", rsa.getFront)
	mux.HandleFunc("/shutdown",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Println("Shutdown pos serv")
			w.Write([]byte("OK")) // Write response body
			if err := s.Shutdown(context.Background()); err != nil {
				log.Fatal(err)
			}
		})

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go s.ListenAndServe()
}
