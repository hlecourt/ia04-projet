package server

import (
	"context"
	"fmt"
	agt "ia04-projet/go/agt"
	"log"
	"net/http"
	"sync"
	"time"
)

type RestBattleServer struct {
	sync.Mutex
	id              string
	addr            string
	MapAgentHP      map[agt.AgentID]int
	sideA           int
	sideB           int
	agentsLeftSideA int
	agentsLeftSideB int
}

func NewRestBattleServerAgent(addr string) *RestBattleServer {
	MapAgentHp := make(map[agt.AgentID]int)
	return &RestBattleServer{id: addr, addr: addr, MapAgentHP: MapAgentHp, sideA: 0, sideB: 0, agentsLeftSideA: 0, agentsLeftSideB: 0}
}

// Test de la méthode
func (rsa *RestBattleServer) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (rsa *RestBattleServer) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	mux.HandleFunc("/initHP", rsa.initHP)
	mux.HandleFunc("/getHP", rsa.returnHP)
	mux.HandleFunc("/attack", rsa.attack)
	mux.HandleFunc("/updateHp", rsa.updateHp)
	mux.HandleFunc("/death", rsa.treatDeath)
	mux.HandleFunc("/mapHP", rsa.returnMapHP)
	mux.HandleFunc("/heal", rsa.heal)
	mux.HandleFunc("/shutdown",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Println("Shutdown battle serv")
			w.Write([]byte("OK")) // Write response body
			if err := s.Shutdown(context.Background()); err != nil {
				log.Fatal(err)
			}
		})

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go s.ListenAndServe()
}
