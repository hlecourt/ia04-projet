package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	clt "ia04-projet/go/client"
	"net/http"
)

func (*RestBattleServer) decodeRequestAttack(r *http.Request) (req clt.ResquestAttack, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestBattleServer) attack(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestAttack(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	if _, found := rsa.MapAgentHP[req.Id]; found {
		rsa.MapAgentHP[req.Id] = rsa.MapAgentHP[req.Id] - req.Damage
		fmt.Println("Agent :", req.Id, "Take :", req.Damage, "Damage")
		if rsa.MapAgentHP[req.Id] <= 0 {
			// delete(rsa.MapAgentHP, req.Id)
			if req.Side == 1 {
				rsa.agentsLeftSideA--
			} else {
				rsa.agentsLeftSideB--
			}
		}
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}
