package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	clt "ia04-projet/go/client"
	"net/http"
)

func (*RestBattleServer) decodeRequestUpdateHp(r *http.Request) (req clt.RequestUpdateHp, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestBattleServer) updateHp(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestUpdateHp(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	var resp clt.ResponseUpdateHp

	if _, found := rsa.MapAgentHP[req.Id]; found {
		resp.HP = rsa.MapAgentHP[req.Id]
		w.WriteHeader(http.StatusOK)
		serial, _ := json.Marshal(resp)
		w.Write(serial)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

}
