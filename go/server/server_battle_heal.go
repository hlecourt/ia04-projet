package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	clt "ia04-projet/go/client"
	"net/http"
)

func (*RestBattleServer) decodeRequestHeal(r *http.Request) (req clt.RequestHeal, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestBattleServer) heal(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestHeal(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	if _, found := rsa.MapAgentHP[req.Id]; found {
		if rsa.MapAgentHP[req.Id] <= 100-req.MaxHeal {
			rsa.MapAgentHP[req.Id] -= req.MaxHeal
			fmt.Println(req.Id, "Healed")
		} else {
			rsa.MapAgentHP[req.Id] = 100
		}
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}
