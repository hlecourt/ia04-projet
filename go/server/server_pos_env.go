package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	agt "ia04-projet/go/agt"
	clt "ia04-projet/go/client"
	"math"
	"net/http"
)

func (*RestMapServer) decodeRequestGetEnv(r *http.Request) (req clt.RequestGetEnv, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestMapServer) getEnv(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestGetEnv(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	var resp clt.ResponseGetEnv
	resp.List_pos = make([]agt.Position, 0)
	resp.List_cnt = make([]agt.CellContent, 0)

	agt_pos := rsa.posManager[req.ID]

	for i := math.Max(0, float64(agt_pos.X-req.SightRange)); i <= math.Min(float64(rsa.sizeBoard)-1, float64(agt_pos.X+req.SightRange)); i++ {
		for j := math.Max(0, float64(agt_pos.Y-req.SightRange)); j <= math.Min(float64(rsa.sizeBoard)-1, float64(agt_pos.Y+req.SightRange)); j++ {
			if rsa.board[int(i)][int(j)].Content != 2 {
				resp.List_pos = append(resp.List_pos, agt.Position{X: int(i), Y: int(j)})
				resp.List_cnt = append(resp.List_cnt, rsa.board[int(i)][int(j)])
			}
		}
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (*RestMapServer) decodeRequestDeath(r *http.Request) (req clt.ResquestDeath, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestMapServer) treatDeath(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestDeath(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	if _, found := rsa.posManager[req.Id]; found {
		pos := rsa.posManager[req.Id]
		rsa.board[pos.X][pos.Y] = agt.CellContent{Content: 2, Fighter: agt.NilFighter()}
		delete(rsa.posManager, req.Id)
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

}
