package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	clt "ia04-projet/go/client"
	"net/http"
)

func (*RestBattleServer) decodeRequestInitHP(r *http.Request) (req clt.RequestInitHP, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestBattleServer) initHP(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestInitHP(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	rsa.MapAgentHP[req.Id] = req.Hp
	if req.Side == 1 {
		rsa.sideA++
		rsa.agentsLeftSideA++
	} else {
		rsa.sideB++
		rsa.agentsLeftSideB++
	}

	fmt.Println("Add :", req.Id, " set Hp :", req.Hp)
	fmt.Println("Side A :", rsa.sideA)
	fmt.Println("Side B :", rsa.sideB)

	w.WriteHeader(http.StatusOK)
}
