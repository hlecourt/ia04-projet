package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	clt "ia04-projet/go/client"
	"net/http"
)

func (*RestBattleServer) decodeRequestGetMapHP(r *http.Request) (req clt.RequestGetMapHP, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestBattleServer) returnMapHP(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	_, err := rsa.decodeRequestGetMapHP(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	var resp clt.ResponseGetMapHP
	resp.MapHP = rsa.MapAgentHP

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}
