package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	clt "ia04-projet/go/client"
	"net/http"
)

func (*RestBattleServer) decodeRequestGetHP(r *http.Request) (req clt.RequestGetHP, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestBattleServer) returnHP(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestGetHP(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	if _, found := rsa.MapAgentHP[req.Id]; found {
		var resp clt.ResponseGetHP
		resp.Hp = rsa.MapAgentHP[req.Id]
		fmt.Println("Agent :", req.Id, "has :", resp.Hp, "Hp")
		w.WriteHeader(http.StatusOK)
		serial, _ := json.Marshal(resp)
		w.Write(serial)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}
