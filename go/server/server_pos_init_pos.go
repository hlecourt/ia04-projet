package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	agt "ia04-projet/go/agt"
	clt "ia04-projet/go/client"
	"math/rand"
	"net/http"
	"time"
)

func (*RestMapServer) decodeRequestInitPosition(r *http.Request) (req clt.RequestPositionInit, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestMapServer) initPosition(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if !rsa.checkMethod("POST", w, r) {
		return
	}

	req, err := rsa.decodeRequestInitPosition(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	var resp clt.ReponsePositionInit
	var tmp agt.Position

	switch req.Side {
	case 1:
		rand.Seed(time.Now().UnixNano())
		tmp = agt.Position{X: rand.Intn(rsa.sizeBoard / 2), Y: rand.Intn(rsa.sizeBoard)}
		for rsa.board[tmp.X][tmp.Y].Content == 0 || rsa.board[tmp.X][tmp.Y].Content == 1 {
			rand.Seed(time.Now().UnixNano())
			tmp = agt.Position{X: rand.Intn(rsa.sizeBoard / 2), Y: rand.Intn(rsa.sizeBoard)}
		}
		resp.Pos = tmp

		rsa.board[tmp.X][tmp.Y] = agt.CellContent{Content: 0,
			Fighter: agt.Fighter{ID: req.ID, Side: 1, Type: agt.TypeSoldier(req.Type)}}

		rsa.nb_soldiers_A++
		rsa.posManager[req.ID] = tmp

	case 2:
		rand.Seed(time.Now().UnixNano())
		tmp := agt.Position{X: rand.Intn(rsa.sizeBoard/2) + rsa.sizeBoard/2, Y: rand.Intn(rsa.sizeBoard)}
		for rsa.board[tmp.X][tmp.Y].Content == 0 || rsa.board[tmp.X][tmp.Y].Content == 1 {
			rand.Seed(time.Now().UnixNano())
			tmp = agt.Position{X: rand.Intn(rsa.sizeBoard/2) + rsa.sizeBoard/2, Y: rand.Intn(rsa.sizeBoard)}
		}
		resp.Pos = tmp
		rsa.board[tmp.X][tmp.Y] = agt.CellContent{Content: 0,
			Fighter: agt.Fighter{ID: req.ID, Side: 2, Type: agt.TypeSoldier(req.Type)}}
		rsa.nb_soldiers_B++
		rsa.posManager[req.ID] = tmp
	default:
		w.WriteHeader(http.StatusNotImplemented)
		return
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}
