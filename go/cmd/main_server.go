package main

import (
	srv "ia04-projet/go/server"
	"log"
)

// this package is used to implement function main, configure parameter

func main() {
	servMain := srv.NewRestMainServerAgent(":8001")
	log.Println("démarrage serveur main...")
	go servMain.Start()
	for true {
	}
}
