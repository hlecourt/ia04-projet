package agt

func NewAgentSoldier(id AgentID, typeSoldier TypeSoldier, side int) Agent {
	var info TypeInfo
	env := make(map[Position]CellContent)

	switch typeSoldier {
	case chevalier:
		info.Type = 0
		info.MaxHealth = 100
		info.Damage = 40
		info.Speed = 20
		info.SightRange = 10
		info.AttackRange = 1
	case archer:
		info.Type = 1
		info.MaxHealth = 100
		info.Damage = 20
		info.Speed = 20
		info.SightRange = 10
		info.AttackRange = 5
	case infirmier:
		info.Type = 2
		info.MaxHealth = 100
		info.Damage = -15
		info.Speed = 20
		info.SightRange = 20
		info.AttackRange = 1
	}
	return Agent{id, info, side, info.MaxHealth, Position{0, 0}, env}
}
