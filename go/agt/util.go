package agt

import (
	"math/rand"
	"time"
)

func RandomTrueByPercentage(percent float32) bool {
	rand.Seed(time.Now().UnixNano())
	tmp := rand.Intn(100)
	if tmp < int(percent*100) {
		return true
	} else {
		return false
	}
}
