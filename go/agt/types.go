package agt

type AgentID string
type TypeSoldier int
type TypeContent int

const (
	chevalier TypeSoldier = 0
	archer    TypeSoldier = 1
	infirmier TypeSoldier = 2
)

const (
	fighter  TypeContent = 0
	obstacle TypeContent = 1
	empty    TypeContent = 2
)

type TypeInfo struct {
	Type        TypeSoldier
	MaxHealth   int
	Damage      int
	Speed       int
	SightRange  int
	AttackRange int
}

type Position struct {
	X int
	Y int
}

type Fighter struct {
	ID   AgentID
	Side int
	Type TypeSoldier
}

type CellContent struct {
	Content TypeContent
	Fighter Fighter
}

type Agent struct {
	ID       AgentID
	Type     TypeInfo
	Side     int
	Health   int
	Position Position
	Env      map[Position]CellContent
}

func NilPosition() Position {
	return Position{-1, -1}
}

func NilFighter() Fighter {
	return Fighter{ID: AgentID("")}
}
